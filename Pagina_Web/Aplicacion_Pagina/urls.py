from django.conf.urls import url
from Aplicacion_Pagina.views import Pagina_Principal
from Aplicacion_Pagina.views import Iniciar_Sesion
from Aplicacion_Pagina.views import Registrar_Usuarios
from Aplicacion_Pagina.views import Recuperar_Contrasena
from Aplicacion_Pagina.views import Mostrar_Equipos
from Aplicacion_Pagina.views import Mostrar_Disponibilidad
from Aplicacion_Pagina.views import Grafico_Individual


urlpatterns = [
    url(r'^inicio$', Pagina_Principal, name='Inicio'),
    url(r'^ingresar$', Iniciar_Sesion, name='Ingresar'),
    url(r'^registrar$', Registrar_Usuarios, name='Registrar'),
    url(r'^recuperar$', Recuperar_Contrasena, name='Recuperar'),
    url(r'^equipos$', Mostrar_Equipos, name='Equipos'),
    url(r'^disponibilidad$', Mostrar_Disponibilidad, name='Disponibilidad'),
    url(r'^grafico_individual?', Grafico_Individual, name='Grafico_Individual')
]
