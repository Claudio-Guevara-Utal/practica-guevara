from django.http import HttpResponse
from django.template import loader
from Aplicacion_Pagina.models import Equipo
from Aplicacion_Pagina.models import Disponibilidad
from datetime import datetime, date, time, timedelta

# Create your views here.


def Pagina_Principal(request):
    template = loader.get_template('index.html')
    context = {}
    today = date.today()
    dia_actual = today.strftime('%Y-%m-%d')
    
    query = Disponibilidad.objects.all().filter(Fecha__gte=dia_actual, Fecha__lte=dia_actual)
    query= query.order_by("Equipo__id","Hora","Fecha")
    
    minuto_actual = 0
    minuto_anterior = 0
    hora_actual = 0
    hora_anterior = 0
    auxiliar_tiempo = 0
    contador_minuto_activo = 0
    contador_minuto_no_activo = 0
    equipo_inicial = 0
    equipo_final = 0
    traspaso = 0
    variable_auxiliar = 0
    lista_activo = []
    lista_no_activo = []
    lista_equipos = []
    for i in query:
        equipo_inicial = i.Equipo.id
        
        if equipo_inicial != variable_auxiliar:
            lista_equipos.append(equipo_inicial)
        
        variable_auxiliar = equipo_inicial

        if equipo_final !=  equipo_inicial and equipo_final != 0:
                lista_activo.append(contador_minuto_activo)
                lista_no_activo.append(contador_minuto_no_activo)
                minuto_actual = 0
                minuto_anterior = 0
                hora_actual = 0
                hora_anterior = 0
                auxiliar_tiempo = 0
                contador_minuto_activo = 0
                contador_minuto_no_activo = 0
        
        hora_actual = i.Hora.hour
        if auxiliar_tiempo == 0:
            minuto_actual = 0
            hora_anterior = hora_actual
            auxiliar_tiempo = auxiliar_tiempo + 1
        else:
            minuto_actual = i.Hora.minute
         
        if i.Tiempo == True:
            if traspaso == 0:
                if hora_actual == hora_anterior:
                    if minuto_actual == 0:
                        contador_minuto_no_activo = contador_minuto_no_activo + minuto_actual
                    else:
                        contador_minuto_no_activo = contador_minuto_no_activo + (minuto_actual-minuto_anterior)
                else:
                    if hora_actual-hora_anterior == 1:
                        contador_minuto_no_activo = contador_minuto_no_activo + (60-minuto_anterior) + minuto_actual
                    else:
                        contador_minuto_no_activo = contador_minuto_no_activo + (hora_actual-hora_anterior-1)*60 + (60-minuto_anterior) + minuto_actual
            else:
                if hora_actual == hora_anterior:
                    if minuto_actual == 0:
                        contador_minuto_activo = contador_minuto_activo + minuto_actual
                    else:
                        contador_minuto_activo = contador_minuto_activo + (minuto_actual-minuto_anterior)
                else:
                    if hora_actual-hora_anterior == 1:
                        contador_minuto_activo = contador_minuto_activo + (60-minuto_anterior) + minuto_actual
                    else:
                        contador_minuto_activo = contador_minuto_activo + (hora_actual-hora_anterior-1)*60 + (60-minuto_anterior) + minuto_actual
            traspaso = 1
        
        else:
            if traspaso == 1:
                if hora_actual == hora_anterior:
                    if minuto_actual == 0:
                        contador_minuto_activo = contador_minuto_activo + minuto_actual
                    else:
                        contador_minuto_activo = contador_minuto_activo + (minuto_actual-minuto_anterior)
                else:
                    if hora_actual-hora_anterior == 1:
                        contador_minuto_activo = contador_minuto_activo + (60-minuto_anterior) + minuto_actual
                    else:
                        contador_minuto_activo = contador_minuto_activo + (hora_actual-hora_anterior-1)*60 + (60-minuto_anterior) + minuto_actual
            else:
                if hora_actual == hora_anterior:
                    if minuto_actual == 0:
                        contador_minuto_no_activo = contador_minuto_no_activo + minuto_actual
                    else:
                        contador_minuto_no_activo = contador_minuto_no_activo + (minuto_actual-minuto_anterior)
                else:
                    if hora_actual-hora_anterior == 1:
                        contador_minuto_no_activo = contador_minuto_no_activo + (60-minuto_anterior) + minuto_actual
                    else:
                        contador_minuto_no_activo = contador_minuto_no_activo + (hora_actual-hora_anterior-1)*60 + (60-minuto_anterior) + minuto_actual
            traspaso = 0
                        

        equipo_final = i.Equipo.id
        hora_anterior = i.Hora.hour
        minuto_anterior = i.Hora.minute

    lista_activo.append(contador_minuto_activo)
    lista_no_activo.append(contador_minuto_no_activo)
    activo = 0
    no_activo = 0
    porcentaje_activo = 0
    minutos_activo = []
    minutos_no_activo = []
    porcentaje = []

    for i in lista_activo:
        activo = activo + i
    
    for i in lista_no_activo:
        no_activo = no_activo + i
   
    if (activo + no_activo) ==0:
        porcentaje_activo = 100
    else:
        porcentaje_activo = ((activo*100)/(activo + no_activo))
        porcentaje_activo = round(porcentaje_activo, 2)
    
    minutos_activo.append(activo)
    minutos_no_activo.append(no_activo)
    porcentaje.append(porcentaje_activo)
    context['Disponibilidad'] = query       
    context['tiempo_activo'] = lista_activo
    context['minutos_activo'] = minutos_activo
    context['minutos_no_activo'] = minutos_no_activo
    context['porcentaje'] = porcentaje
    context['Equipos'] = lista_equipos 
    return HttpResponse(template.render(context, request))

def Iniciar_Sesion(request):
    template = loader.get_template('login.html')
    context = {}

    return HttpResponse(template.render(context, request))


def Registrar_Usuarios(request):
    template = loader.get_template('register.html')
    context = {}

    return HttpResponse(template.render(context, request))


def Recuperar_Contrasena(request):
    template = loader.get_template('password.html')
    context = {}

    return HttpResponse(template.render(context, request))


def Mostrar_Equipos(request):
    template = loader.get_template('servers.html')
    context = {}
    Equipo_Registrado = Equipo.objects.all()
    context['Equipo'] = Equipo_Registrado

    return HttpResponse(template.render(context, request))


def Mostrar_Disponibilidad(request):
    template = loader.get_template('disponibilidad.html')
    context = {}
    Disponibilidad_Equipo = Disponibilidad.objects.all()
    context['Disponibilidad'] = Disponibilidad_Equipo

    return HttpResponse(template.render(context, request))



def Grafico_Individual(request):
    template = loader.get_template('grafico.html')
    if request.method == "GET":
        _get = request.GET
        hora1 = _get.get("hora_ini")
        hora2 = _get.get("hora_fin")
        hora = datetime.strptime(hora1, '%H:%M:%S')
        hora = hora.minute 
        _id_equipo = _get.get("equipo")
        Fecha_Inicio = _get.get("fechaini")
        datetime.strptime(Fecha_Inicio, '%Y-%m-%d')
        Fecha_Fin = _get.get("fechafin")
        datetime.strptime(Fecha_Fin, '%Y-%m-%d')
        query = Disponibilidad.objects.all().filter(Fecha__gte=Fecha_Inicio,
                                                    Fecha__lte=Fecha_Fin,
                                                    Equipo__id=_id_equipo,Hora__gte=hora1, Hora__lte=hora2)
        
        query = query.order_by("Fecha", "Hora")
        context = {}
        context['Disponibilidad'] = query
    
    return HttpResponse(template.render(context, request))
