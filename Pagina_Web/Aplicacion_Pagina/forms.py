from django import forms


class DateInput(forms.DateInput):
    input_type = 'date'


class ExampleForm(forms.Form):
    date_forms = forms.DateField(widget=DateInput)


class ExampleModelForm(forms.Form):
    class Meta:
        widgets = {'date_forms': DateInput()}
