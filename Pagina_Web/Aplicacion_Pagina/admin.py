from django.contrib import admin
from Aplicacion_Pagina.models import Equipo
from Aplicacion_Pagina.models import Disponibilidad

# Register your models here.


class EquipoAdmin(admin.ModelAdmin):
    list_display = ("id", "Nombre", "Funcion", "Detalle")
    list_filter = ("Nombre", )


class DisponibilidadAdmin(admin.ModelAdmin):
    list_display = ("id", "Equipo", "Tiempo", "Hora", "Fecha")
    list_filter = ("Equipo", )
    date_hierarchy = 'Fecha'


admin.site.register(Equipo, EquipoAdmin)


admin.site.register(Disponibilidad, DisponibilidadAdmin)
