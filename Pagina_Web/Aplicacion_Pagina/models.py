from django.db import models

# Create your models here.


class Equipo(models.Model):
    Nombre = models.CharField(max_length=50)
    Funcion = models.CharField(max_length=50)
    Detalle = models.CharField(max_length=50)
    Host = models.CharField(max_length=50)

    def __str__(self):
        return self.Nombre


class Disponibilidad(models.Model):
    id = models.AutoField(primary_key = True)
    Equipo = models.ForeignKey(Equipo, on_delete=models.CASCADE)
    Tiempo = models.BooleanField(default=True)
    Hora = models.TimeField()
    Fecha = models.DateField(auto_now=False)
