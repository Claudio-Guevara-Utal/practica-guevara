# Generated by Django 3.0.2 on 2020-01-09 12:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Aplicacion_Pagina', '0004_auto_20200108_1914'),
    ]

    operations = [
        migrations.AlterField(
            model_name='disponibilidad',
            name='Fecha',
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name='disponibilidad',
            name='Hora',
            field=models.CharField(max_length=50),
        ),
    ]
