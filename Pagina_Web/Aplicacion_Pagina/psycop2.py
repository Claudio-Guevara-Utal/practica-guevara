import psycopg2
import subprocess as sp
from datetime import datetime, date, time, timedelta
import threading
import time
try:
    connection = psycopg2.connect(user = "claudio",
                                  password = "321",
                                  host = "127.0.0.1",
                                  port = "5432",
                                  database = "pagina")

    cursor = connection.cursor()
    
    class myThread (threading.Thread):
        def __init__(self, id_equipo, host):
            threading.Thread.__init__(self)
            self.id_equipo = id_equipo
            self.host = host
        def run(self):
            agregar_datos(self.id_equipo, self.host)

    def agregar_datos(id_equipo, host):
        today = date.today()
        hour = datetime.now()
        fecha = today.strftime('%Y-%m-%d')
        hora = hour.strftime('%H:%M:%S')

        status,result = sp.getstatusoutput("ping -c1 -w2 " + host)

        postgres_insert_query = """ INSERT INTO "Aplicacion_Pagina_disponibilidad" ("Tiempo", "Hora", "Fecha", "Equipo_id") VALUES (%s,%s,%s,%s)"""	
	
        if status == 0:
            record_to_insert = ('1', hora, fecha, id_equipo)
        else:
            record_to_insert = ('0', hora, fecha, id_equipo)

        cursor.execute(postgres_insert_query, record_to_insert)

    postgreSQL_select_Query = """ select id, "Host" from "Aplicacion_Pagina_equipo" """
    cursor.execute(postgreSQL_select_Query)
    equipos = cursor.fetchall()

    for ip in equipos:
        t1 = myThread(ip[0], ip[1])
        t1.start()
    t1.join()       
    connection.commit()

except (Exception, psycopg2.Error) as error :
    print ("Error while connecting to PostgreSQL", error)
finally:
    #closing database connection.
        if(connection):
            cursor.close()
            connection.close()
            print("PostgreSQL connection is closed")
